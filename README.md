# Google-Consent-Cookie-Script

Setzt einige Cookies, damit die Consent-Abfrage bei Google nicht mehr erscheint. Sinnvoll, wenn alle Cookies nach der Sitzung gelöscht werden. 

# Installation

1. Einen User-Script-Manager für seinen Browser installieren, z.B. Greasmonkey oder Tampermonkey. 
1. Das [Script](https://codeberg.org/rufposten/Google-Consent-Cookie-Script/src/branch/main/google-consent-cookie-setzen.user.js) per Copy & Paste als neues User-Script anlegen

# Funktionsweise
Das Script setzt beim Aufruf einer Seite von google.de drei Cookies, die einem Ablehnen aller Cookies entsprechen und lädt die Seite neu. Dann erscheint keine Consent-Abfrage mehr. 

# Hinweis
Das Script funktioniert möglicherweise nicht sonderlich lange. 