// ==UserScript==
// @name         Google-Consent-Cookie setzen
// @version      1.0.0
// @description  Setzt einige Cookies, damit die Consent-Abfrage bei Google nicht mehr erscheint. Sinnvoll, wenn alle Cookies nach der Sitzung gelöscht werden
// @author       rufposten
// @match        *://www.google.de/*
// @match        *://www.google.com/*
// @run-at       document-start
// ==/UserScript==

if (!document.cookie.match("(^|;)\\s*CONSENT=YES\\+")) {
  document.cookie="CONSENT=YES+shp.gws-20210526-0-RC2.de+FX+949";
  document.cookie="ANID=OPT_OUT";
  document.cookie="1P_JAR=2021-05-30-09";
  location.reload();
}